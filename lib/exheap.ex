defmodule BinomialHeap do
    @moduledoc ~S"""
    Binomial Heap (binomial priority queue).
    """

    @doc ~S"""
    Creates new Heap

    ## Examples

        iex> h = BinomialHeap.new
        []
    """
    def new do emptyHeap end

    @doc ~S"""
    Checks is Heap empty

    ## Examples
        
        iex> h = BinomialHeap.new
        []
        iex> BinomialHeap.is_empty? h
        true
        iex> h = BinomialHeap.push h, 1
        [{0, 1, []}]
        iex> BinomialHeap.is_empty? h
        false
    """
    def is_empty?(heap) do 
        heapEmpty?(heap) 
    end


    @doc ~S"""
    Push value to Heap

    ## Examples
        
        iex>  h = BinomialHeap.new
        []
        iex>  h = BinomialHeap.push h, 1
        [{0, 1, []}]
        iex>  h = BinomialHeap.push h, 2
        [{1, 1, [{0, 2, []}]}]
    """
    def push(heap, x) do
        insHeap(x, heap)
    end

    @doc ~S"""
    Get min value from Heap, doesn't remove value from Heap

    ## Examples
        
        iex>  h = BinomialHeap.new
        []
        iex>  h = BinomialHeap.push h, 1
        [{0, 1, []}]
        iex>  h = BinomialHeap.push h, 2
        [{1, 1, [{0, 2, []}]}]
        iex> BinomialHeap.get! h
        1
    """
    def get([]) do nil end
    def get(heap) do
        findHeap(heap)
    end


    @doc ~S"""
    Same as BinomialHeap.get, but rases error on empty Heap

    ## Examples
        
        iex>  h = BinomialHeap.new       
        []
        iex> BinomialHeap.get! h
        ** (RuntimeError) Heap is empty
            heap.ex:13: BinomialHeap.get!/1
    """
    def get!([]) do raise "Heap is empty" end
    def get!(heap) do get(heap) end


    @doc ~S"""
    Take min value from Heap and removes it

    ## Examples
        
        iex>  h = 1..2 |> BinomialHeap.heapyfy
        [{1, 1, [{0, 2, []}]}]
        iex>  {value, h} = BinomialHeap.pop h
        {1, [{0, 2, []}]}
        iex>  {value, h} = BinomialHeap.pop h
        {2, []}
    """
    def pop([]) do {nil, []} end
    def pop(heap) do
        {findHeap(heap), delHeap(heap)}
    end

    @doc ~S"""
    Same as BinomialHeap.pop, but rases error on empty Heap

    ## Examples
        
        iex>  h = 1..1 |> BinomialHeap.heapyfy
        [{0, 1, []}]
        iex>  {value, h} = BinomialHeap.pop! h
        {1, []}
        iex>  {value, h} = BinomialHeap.pop! h
        ** (RuntimeError) Heap is empty
            heap.ex:82: BinomialHeap.pop!/1
    """
    def pop!([]) do raise "Heap is empty" end
    def pop!(heap) do pop(heap) end

    @doc ~S"""
    Create Heap from enumerable

    ## Examples
        
        iex>  1..2 |> BinomialHeap.heapyfy
        [{1, 1, [{0, 2, []}]}]
        iex> %{a: 1, b: 2} |> BinomialHeap.heapyfy
        [{1, {:a, 1}, [{0, {:b, 2}, []}]}]
    """
    def heapyfy(list) do
        Enum.reduce list, BinomialHeap.new, 
            fn(x, acc) -> BinomialHeap.push(acc, x) end
    end

    @doc ~S"""
    Create ordered List from Heap

    ## Examples
        
        iex>  h = 1..2 |> BinomialHeap.heapyfy
        [{1, 1, [{0, 2, []}]}]
        iex> BinomialHeap.to_list h
        [1, 2]

        iex>  h = [{3, 10}, {2, 50}, {1, 100}, {4, 100}] |> BinomialHeap.heapyfy
        [{2, {1, 100}, [{1, {2, 50}, [{0, {3, 10}, []}]}, {0, {4, 100}, []}]}]
        iex> BinomialHeap.to_list h
        [{1, 100}, {2, 50}, {3, 10}, {4, 100}]
    """
    def to_list(heap) do
        to_list(heap, [])
    end



    ### 
    #  Private section of black magick
    ###

    defp to_list([], list) do Enum.reverse(list) end
    defp to_list(heap, list) do 
        {value, heap} = pop(heap)
        to_list(heap, [value|list])
    end


    defp emptyHeap do [] end
    defp heapEmpty?([]) do true end
    defp heapEmpty?(_) do false end
    defp rank({heap_rank, _, _}) do heap_rank end
    defp root({_, value, _}) do value end

    defp link({rank1, value1, tail1}, {rank2, value2, tail2}) when value1 <= value2 do
        {rank1+1, value1, [{rank2, value2, tail2}|tail1]}
    end

    defp link({rank1, value1, tail1}, {_, value2, tail2}) do
        {rank1+1, value2, [{rank1, value1, tail1}|tail2]}
    end

    defp insTree(tree, []) do [tree] end
    defp insTree(tree, [tree1|tail1]) do
        case rank(tree) < rank(tree1) do
            true -> [tree|[tree1|tail1]]
            false -> insTree(link(tree, tree1), tail1)
        end
    end

    defp insHeap(x, []) do [{0, x, []}] end
    defp insHeap(x, ts) do 
        insTree({0, x, []}, ts)
    end

    defp merge(tree,[]) do tree end
    defp merge([], tree) do tree end
    defp merge([tree1|tail1], [tree2|tail2]) do
        cond do
           rank(tree1) < rank(tree2) -> 
                [tree1|merge(tail1, [tree2|tail2])]
           rank(tree1) > rank(tree2) -> 
                [tree1|merge([tree1|tail1], tail2)]
           rank(tree1) == rank(tree2) -> 
                insTree(link(tree1, tree2), merge(tail1, tail2))
        end
    end

    defp removeMinTree([]) do [] end
    defp removeMinTree([tree]) do {tree,[]} end
    defp removeMinTree([tree|tail]) do
        {tree1, tail1} = removeMinTree(tail)
        case root(tree) < root(tree1) do
            true -> {tree,tail}
            false -> {tree1,[tree|tail1]}
        end
    end

    defp findHeap(tree) do 
        {head, _} = removeMinTree(tree)
        root(head) 
    end

    defp delHeap(tree) do  
        {{_, _, tail1}, tail2} = removeMinTree(tree)
        merge(Enum.reverse(tail1), tail2)
    end
end