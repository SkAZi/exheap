defmodule ExHeap.Mixfile do
  use Mix.Project

  def project do
    [app: :exheap,
     version: "0.0.1",
     elixir: "~> 0.14.1",
     name: "ExHeap",
     source_url: "https://github.com/SkAZi/ExHeap"
    ]
  end

end
