defmodule ExHeap.BinomialHeap.Test do
  use ExUnit.Case, async: true
  doctest BinomialHeap

  test "speed test" do
    {time, _} = :timer.tc fn -> 
        1..1_000 
        |> BinomialHeap.heapyfy
        |> BinomialHeap.to_list 
    end
    assert time < 5_000

    {time, _} = :timer.tc fn -> 
        1..10_000 
        |> BinomialHeap.heapyfy
        |> BinomialHeap.to_list 
    end
    assert time < 50_000

    {time, _} = :timer.tc fn -> 
        1..100_000 
        |> BinomialHeap.heapyfy
        |> BinomialHeap.to_list 
    end
    assert time < 500_000
  end
end